<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @deprecated 4.0 Utiliser spip-cli pour tout réindexer…
 */
function action_indexer_tout_reindexer_async() {
	include_spip('inc/indexer');
	indexer_tout_reindexer_async();
}
