# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Fixed

- Reconnecter le serveur régulièrement (120s) lors d’indexations longues
- Reconnecter le serveur si on voit une erreur "has gone away" (et retenter la requête)
- Mieux nettoyer les données d’auteurs à indexer

## 4.0.1 - 2024-09-16

### Fixed

- Test de connexion du moteur Sphinx

## 4.0.0 - 2024-09-16

### Added

- Liste des contenus à indexer via des générateurs (interface `SourceGeneratorInterface`)

### Changed

- Refonte de l’indexation en CLI
- Refonte de l’indexation dans l’espace privé (ne fonctionne maintenant qu’avec les sources implémentant `SourceGeneratorInterface`)
- Refonte de la présentation des pages d’indexation dans l’espace privé
- Nécessite SPIP >= 4.1 & PHP >= 7.4

### Fixed

- Valeur du grand total des paginations, avec le moteur Manticore
- #6 Snippets en erreur avec le moteur Manticore

### Deprecated

- l’interface `SourceInterface`, au profit de `SourceGeneratorInterface`. Implémenter les deux pour le moment.
- l’indexation complète asyncrhrone (utiliser spip-cli)

## 3.1.1 - 2024-08-02

### Added

- Composerisation du plugin

## 3.0.4 - 2023-06-10

### Changed

- Compatibilité SPIP 4.2 (plus précisement 4.2.3)
