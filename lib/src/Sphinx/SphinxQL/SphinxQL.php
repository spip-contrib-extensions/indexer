<?php

namespace Sphinx\SphinxQL;

class SphinxQL
{
	private $host;

	private $port;

	/**
	 * @var int seconds
	 */
	protected $reconnect_duration = 120;

	/**
	 * @var \MySQLi
	 */
	private $sql;

	public function __construct($host = '127.0.0.1', $port = 9306) {
		$this->host = $host;
		$this->port = $port;
		$this->connect();
	}

	/**
	 * Se connecter à Sphinx
	 * @return bool
	 **/
	public function connect() {
		// date de dernière de connexion
		static $time = null;
		$time ??= time();
		if (!$this->sql) {
			$time = time();
			return $this->reconnect();
		}
		if (($time + $this->reconnect_duration) >= time()) {
			return true;
		}

		$time = time();
		return $this->reconnect();
	}

	public function reconnect(): bool {
		if ($this->sql) {
			spip_log('Reconnexion à Sphinx / Manticore', 'indexer.' . _LOG_INFO_IMPORTANTE);
			$this->sql->close();
			$this->sql = null;
			usleep(100000);
		}

		try {
			mysqli_report(MYSQLI_REPORT_STRICT);
			$this->sql = new \MySQLi($this->host, null, null, null, $this->port);
		} catch (\Exception $e) {
			spip_log('Connexion erronée pour host : ' . $this->host . ' , port: ' . $this->port, 'indexer.' . _LOG_ERREUR);
			spip_log($e->getMessage(), 'indexer.' . _LOG_ERREUR);
			$this->sql = null;
			return false;
		} finally {
			mysqli_report(MYSQLI_REPORT_OFF);
		}

		if ($this->sql->connect_error) {
			spip_log('Échec de connexion ', 'indexer.' . _LOG_INFO_IMPORTANTE);
			spip_log($this->sql->connect_error, 'indexer.' . _LOG_ERREUR);
			return false;
		}

		return true;
	}

	/**
	 * Exécute une requête
	 **/
	public function query($query) {
		#spip_log((empty($_SERVER['REQUEST_URI']) ? '(cli)' : $_SERVER['REQUEST_URI']) .' | ' . $query, 'indexer' . _LOG_DEBUG);

		// Connexion (ou reconnexion)
		if (!$this->connect()) {
			return false;
		}

		return $this->sql->multi_query($query);
	}

	/**
	 * Échappe une chaîne
	 **/
	public function escape_string($string) {
		if (!$this->sql || $this->sql->connect_errno) {
			return false;
		}

		return $this->sql->escape_string($string);
	}

	/**
	 * Récupère les dernières erreurs
	 **/
	public function errors() {
		if (!$this->sql) {
			return false;
		}
		if (isset($this->sql->error_list)) {
			return $this->sql->error_list;
		}
		return [];
	}

	/**
	 * Récupère toutes les informations de la requête ET ses metas
	 **/
	public function allfetsel($query) {
		if (!$this->sql) {
			return false;
		}

		$liste = [
			'docs' => [],
			'facets' => [],
			'meta' => [],
			'query' => $query,
		];

		try {
			$docs = $this->query($query);

			// les jeux de réponses sont les suivants :
			// 1) les documents trouvés
			// 2+) les FACET à la suite
			$reponses = [];
			do {
				if ($result = $this->sql->store_result()) {
					$a = [];
					while ($row = $result->fetch_assoc()) {
						$a[] = $row;
					}
					$reponses[] = $a;
					$result->free();
				}
			} while ($this->sql->more_results() && $this->sql->next_result());

			$liste['docs'] = array_shift($reponses);
			$liste['facets'] = $this->parseFacets($reponses);
		} catch (\Exception $e) {
			echo "\n<div><tt>",htmlspecialchars($query),"</tt></div>\n";
			var_dump($e->getMessage());
			return false;
		}

		// recuperer les META
		if ($meta = $this->sql->query('SHOW META')) {
			$a = [];
			while ($row = $meta->fetch_assoc()) {
				$a[] = $row;
			}
			$liste['meta'] = $this->parseMeta($a);
		} else {
			$liste['meta'] = ['error' => 'meta illisible'];
		}

		return ['query' => $liste];
	}

	/**
	 * Transforme un tableau de FACET en tableau PHP utilisable
	 *
	 * @param array $facettes
	 * @return array
	 **/
	public function parseFacets($facettes) {
		$facets = [];
		if (is_array($facettes)) {
			foreach ($facettes as $facette) {
				foreach ($facette as $i => $desc) {
					$nb = $desc['count(*)'];
					unset($desc['count(*)']);
					$key = array_keys($desc);
					$key = reset($key);
					$value = array_shift($desc);
					if (count($desc)) {
						var_dump($desc);
						die('Contenu non pris en compte dans FACET !');
					}
					if ($i == 0) {
						$facets[$key] = [];
					}
					$facets[$key][$value] = $nb;
				}
			}
		}

		return $facets;
	}

	/**
	 * Transforme un tableau des Metas en tableau PHP élaboré
	 *
	 * Regroupe entre autres les infos de keywords
	 */
	public function parseMeta($metas) {
		$liste = [];
		foreach ($metas as $meta) {
			$cle = $meta['Variable_name'];
			$val = $meta['Value'];
			// cles keywords[0] ...
			if (substr($cle, -1, 1) == ']') {
				[$cle, $index] = explode('[', $cle);
				$index = rtrim($index, ']');

				if (!isset($liste[$cle])) {
					$liste[$cle] = [];
				}

				$liste[$cle][$index] = $val;
			} else {
				$liste[$cle] = $val;
			}
		}

		if (isset($liste['keyword'])) {
			$liste['keywords'] = [];
			foreach ($liste['keyword'] as $index => $key) {
				$liste['keywords'][$key] = [
					'keyword' => $key,
					'docs' => $liste['docs'][$index],
					'hits' => $liste['hits'][$index],
				];
			}
			unset($liste['keyword'], $liste['docs'], $liste['hits']);
		}

		return $liste;
	}
}