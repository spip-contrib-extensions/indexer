<?php

namespace Spip\Indexer\Sources;
use Indexer\Sources\SourceGeneratorInterface;

defined('_INDEXER_PARTS_NUMBER') || define('_INDEXER_PARTS_NUMBER', 1000);

use Indexer\Indexer;
use Indexer\Sources\SourceInterface;
use Indexer\Sources\Sources;

class SpipSourcesIndexer
{
	/**
	 * @var \Indexer\Indexer
	 */
	private $indexer = null;

	/**
	 * @var \Indexer\Sources\Sources
	 */
	private $sources = null;

	/**
	 * @var string clé de config
	 */
	private $meta_stats = 'indexer/indexing/stats';

	public function __construct(Indexer $indexer, Sources $sources) {
		$this->indexer = $indexer;
		$this->sources = $sources;
		$this->initTimeout();
	}


	public function isTimeout(): bool {
		static $timeout = null;

		if ($timeout === null) {
			include_spip('base/upgrade');

			if (defined('_TIME_OUT')) {
				$timeout = _TIME_OUT;
			} else {
				$timeout = time() + _UPGRADE_TIME_OUT;
			}
		}

		return time() >= $timeout;
	}

	public function initTimeout() {
		$this->isTimeout(); // le premier lancement initialise les temps
	}

	public function isStarted(): bool {
		$stats = $this->loadIndexesStats();

		return $stats['indexing'] > 0;
	}

	public function startedTime(): int {
		$stats = $this->loadIndexesStats();

		return $stats['indexing'] > 0 ? $stats['timestamp'] : 0;
	}

	/**
	 * @return array{indexing: int, timestamp: int, last_source: string, sources: array<string, array>}
	 */
	public function loadIndexesStats() {
		include_spip('inc/config');

		$stats = lire_config($this->meta_stats, []);
		if (!is_array($stats)) {
			$stats = [];
		}

		return $stats + [
			'indexing' => 0,
			'timestamp' => time(),
			'last_source' => '',
			'sources' => [],
		];
	}

	public function loadIndexesStatsClean() {
		$stats = $this->loadIndexesStats();

		return $stats;
	}

	public function saveIndexesStats($stats) {
		include_spip('inc/config');
		ecrire_config($this->meta_stats, $stats);
	}

	public function resetIndexesStats() {
		include_spip('inc/config');
		effacer_config($this->meta_stats);
	}

	/**
	 * @return array{ok: bool, stats: array{last: int, items: int, total: int}, message: string, reload: bool}
	 */
	public function indexerSource(SourceGeneratorInterface $source): array {

		$stats = $this->loadIndexesStatsClean();
		$me = get_class($source); // PHP 8+ $source::class;
		$stats['last_source'] = $me;


		if ($stats['indexing'] === 0) {
			// indiquer rapidement qu’une indexation est en cours
			$stats['indexing'] = 1;
			$this->saveIndexesStats($stats);
		}

		$stats['sources'][$me]['last'] ??= 0;
		$stats['sources'][$me]['items'] ??= 0;
		$stats['sources'][$me]['total'] ??= $source->countAllDocuments();

		// les hierarchies ont id_objet = 'rubriques' !
		$next = $stats['sources'][$me]['last'];
		if ($next && is_int($next)) {
			$next++;
		}

		$documents = $source->yieldDocuments($next);
		$ok = true;
		$errors = 0;
		$nb = 0;
		foreach ($documents as $document) {
			$_ok = $this->indexer->replaceDocument($document);
			$ok &= $ok;
			$stats['sources'][$me]['last'] = $document->properties['id_objet'];
			$stats['sources'][$me]['items']++;
			++$nb;
			if (!$_ok) {
				$ok = false;
				spip_log(sprintf('Echec d’indexation du document %s : %s', $document->id, $document->title), 'indexer');
				if (++$errors > 20) {

					$this->saveIndexesStats($stats);
					return [
						'ok' => false,
						'stats' => $stats['sources'][$me],
						'message' => 'Trop d’erreurs d’indexations… On coupe la boucle !',
						'reload' => false,
					];
				}
			}

			if ($this->isTimeout() || $nb >= 1000) {
				$this->saveIndexesStats($stats);
				return [
					'ok' => true,
					'stats' => $stats['sources'][$me],
					'message' => 'Reload',
					'reload' => true,
				];
			}
		}

		$this->saveIndexesStats($stats);
		return [
			'ok' => true,
			'stats' => $stats['sources'][$me],
			'message' => 'Terminé',
			'reload' => false,
		];
	}
}
