<?php

namespace Indexer\Sources;

interface SourceGeneratorInterface
{
	public function __toString();

	/**
	 * Retourne les documents ayant certaines conditions
	 *
	 * @param mixed $start     Condition qui remplira `$column >= $start`
	 * @param mixed $end       Condition qui remplira `$column < $end`
	 * @param string $column   Colonne affectée
	 * @return \Generator<Document>
	 */
	public function yieldDocuments($start = 0, $end = 0, $column = ''): \Generator;

	/**
	 * Retourne l’ensemble des documents
	 *
	 * @return \Generator<Document>
	 */
	public function yieldAllDocuments(): \Generator;

	/**
	 * Retourne le nombre de documents ayant certaines conditions
	 *
	 * @param mixed $start     Condition qui remplira `$column >= $start`
	 * @param mixed $end       Condition qui remplira `$column < $end`
	 * @param string $column   Colonne affectée
	 */
	public function countDocuments($start = 0, $end = 0, $column = ''): int;

	/**
	 * Retourne le nombre de l’ensemble des documents
	 */
	public function countAllDocuments(): int;
}
