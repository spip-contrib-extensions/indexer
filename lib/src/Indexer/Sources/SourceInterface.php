<?php

namespace Indexer\Sources;

/**
 * @deprecated 4.0 use SourceGeneratorInterface
 */
interface SourceInterface
{
	public function __toString();

	public function getDocuments($start = 0, $end = 0, $column = '');

	public function getAllDocuments();

	/**
	 * Indique le nombre de découpages pour indexer, en prenant $count éléments à chaque fois
	 * @param int $count */
	public function getParts($count);
}
