<?php

namespace Indexer\Sources;

class Sources implements \IteratorAggregate, \Countable
{
	/**
	 * @var SourceInterface|SourceGeneratorInterface[]
	 */
	private $sources = [];

	public function __construct() {
	}

	public function has(string $key): bool {
		return array_key_exists($key, $this->sources);
	}

	public function get(string $key): ?SourceInterface {
		return $this->sources[$key] ?? null;
	}

	public function register($cle, SourceInterface $source) {
		$this->sources[$cle] = $source;
	}

	public function unregister($cle) {
		unset($this->sources[$cle]);
	}

	/**
	 * @deprecated 4.0 use get()
	 * @param mixed $cle
	 * @return SourceInterface|null
	 */
	public function getSource($cle) {
		return $this->get($cle);
	}

	#[\ReturnTypeWillChange]
	public function getIterator() {
		return new \ArrayIterator($this->sources);
	}

	public function count(): int {
		return count($this->sources);
	}
}
