<?php

namespace Indexer\Storage;

use Indexer\Sources\Document;

interface StorageInterface
{
	public function replaceDocument(Document $document);

	/**
	 * @param Document[] $documents
	 */
	public function replaceDocuments($documents);

	/**
	 * @param none $source
	 */
	public function purgeDocuments($source);
}
