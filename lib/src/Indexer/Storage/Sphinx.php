<?php

namespace Indexer\Storage;

use Indexer\Sources\Document;
use Sphinx\SphinxQL\SphinxQL;

class Sphinx implements StorageInterface
{
	/**
	 * @var SphinxQL|null
	 */
	private $sphinxql = null;

	/**
	 * @var string Nom de l'index
	 */
	private $indexName = '';

	public function __construct(SphinxQL $sphinxql, $indexName) {
		$this->sphinxql = $sphinxql;
		$this->indexName = $indexName;
	}

	public function replaceDocuments($documents) {
		include_spip('inc/config');
		$this->sphinxql->connect(); // reconnecter en cas de timeout

		if (defined('_INDEXER_READONLY') && _INDEXER_READONLY) {
			spip_log(sprintf('READONLY (devait indexer %s documents)', count($documents)), 'indexer.' . _LOG_DEBUG);
			return true;
		}


		// insertion document par document
		// il semble que sphinxql n'aime pas plusieurs lignes d'un coup.
		foreach ($documents as $document) {
			// On vérifie qu'il y a bien un Document
			if ($document && $document instanceof Document) {
				if (!$this->_replaceDocument($document)) {
					$errors = $this->sphinxql->errors();
					if (!$errors) {
						return false;
					}
					$lastError = end($errors);
					spip_log($lastError, 'indexer.' . _LOG_ERREUR);

					if (false !== strpos($lastError['error'], 'has gone away')) {
						spip_log('Tentative de reconnexion au serveur Sphinx / Manticore', 'indexer.' . _LOG_INFO_IMPORTANTE);
						if (!$this->sphinxql->reconnect()) {
							return false;
						}
						if (!$this->_replaceDocument($document)) {
							return false;
						}
					}
				}
			}
		}

		return true;
	}

	protected function _replaceDocument(Document $document): bool {
		$query = "
			REPLACE INTO $this->indexName
				(id,  title, summary, content, date, date_indexation, uri, properties, signature)
			VALUES
		";

		// Effacer les documents ayant un drapeau "to_delete"
		if ($document->to_delete === true) {
			$q = "DELETE FROM $this->indexName WHERE id=" . $document->id;
		} else {
			$data = $this->reformatDocument($document);
			$data = array_map([$this->sphinxql, 'escape_string'], $data);
			$q = $query . "('" . implode("', '", $data) . "')";
		}

		return (bool) $this->sphinxql->query($q);
	}

	public function replaceDocument(Document $document) {
		return $this->replaceDocuments([$document]);
	}

	public function reformatDocument(Document $document) {
		// Indexation specifique des champs de date sous forme de nombres
		// dans properties.ymd -- https://contrib.spip.net/Indexer-date-32bits
		$dateu = strtotime($document->date);
		if ($dateu) {
			$current_timezone = date_default_timezone_get();
			date_default_timezone_set('UTC');
			// recalculer dateu pour les dates floues: 2000-00-00 => 2000-01-01
			$dateu = strtotime(str_replace('-00', '-01', $document->date));
			$document->properties['ymd'] = [
				'year' => intval(date('Y', $dateu)),
				'yearmonth' => intval(date('Ym', $dateu)),
				'yearmonthday' => intval(date('Ymd', $dateu)),
				'u' => $dateu,
				'datetime' => $document->date,
			];
			if ($current_timezone) {
				date_default_timezone_set($current_timezone);
			}
		}

		return [
			'id' => $document->id,
			'title' => $document->title,
			'summary' => $document->summary,
			'content' => $document->content,
			'date' => $dateu,
			'date_indexation' => intval($document->date_indexation),
			'uri' => $document->uri,
			'properties' => json_encode($document->properties),
			'signature' => $this->signer($document),
		];
	}

	public function signer($doc) {
		include_spip('inc/securiser_action');
		return md5(secret_du_site() . json_encode($doc));
	}

	public function purgeDocuments($source = null) {
		include_spip('inc/config');
		$source = lire_config('indexer/source', lire_config('adresse_site'));

		$q = "DELETE FROM $this->indexName";
		if ($source) {
			$q .= ' WHERE properties.source=' . _q($source);
		}
		if (!$this->sphinxql->query($q)) {
			spip_log($this->sphinxql->errors(), 'indexer');
			spip_log($q, 'indexer');
			return false;
		}
		return true;
	}
}
