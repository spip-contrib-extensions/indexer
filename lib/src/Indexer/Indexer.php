<?php

namespace Indexer;

use Indexer\Sources\Document;
use Indexer\Storage\StorageInterface;

class Indexer
{
	/**
	 * @var StorageInterface|null
	 */
	private $storage = null;

	public function __construct() {
	}

	public function registerStorage(StorageInterface $storageEngine) {
		$this->storage = $storageEngine;
	}

	public function replaceDocument(Document $document) {
		return $this->storage->replaceDocument($document);
	}

	public function replaceDocuments($documents) {
		return $this->storage->replaceDocuments($documents);
	}

	public function purgeDocuments($source = null) {
		return $this->storage->purgeDocuments($source);
	}
}
