<?php

use Indexer\Sources\SourceGeneratorInterface;
use Spip\Indexer\Sources\SpipSourcesIndexer;

function formulaires_indexer_contenus_charger_dist() {
	if (!autoriser('indexer')) {
		return false;
	}

	include_spip('inc/indexer');
	$sources = indexer_sources_with_generator();
	$_sources = array_keys(iterator_to_array($sources));

	# on ne gère plus cela en interface web
	$_sources_without_generator = [];
	foreach (indexer_sources() as $key => $source) {
		if (!$source instanceof SourceGeneratorInterface) {
			$_sources_without_generator[] = $key;
		}
	}

	$SpipSourceIndexer = new SpipSourcesIndexer(indexer_indexer(), $sources);
	$started = $SpipSourceIndexer->isStarted();

	return [
		'source' => _request('source'),
		'sources' => $_sources,
		'sources_without_generator' => $_sources_without_generator,
		'run' => _request('run'),
		'all' => _request('all'),

		'current_source_index' => (int) array_search(_request('source'), $_sources) + 1,
		'current_source' => _request('current_source'),
		'current_total' => _request('current_total'),
		'current_items' => _request('current_items'),

		'editable' => true,
		'started' => $started,
		'started_time' => $SpipSourceIndexer->startedTime(),
	];
}

function formulaires_indexer_contenus_verifier_dist() {
	$run = _request('run') ?: 'start';
	set_request('run', $run);

	if ($run === 'end' || $run === 'purge') {
		return [];
	}

	include_spip('inc/indexer');
	$indexer = indexer_indexer();
	$sources = indexer_sources_with_generator();

	$source = _request('source');
	if ($run === 'start' && $source === null) {
		set_request('all', 'all');
		$source = array_keys(iterator_to_array($sources))[0];
		set_request('source', $source);
		set_request('current_source_index', 0);
		ecrire_meta('indexer_derniere_reindexation', time());
	}

	$SpipSourcesIndexer = new SpipSourcesIndexer($indexer, $sources);

	// arrêt forcé
	if ($run === 'stop') {
		$SpipSourcesIndexer->resetIndexesStats();
		return [];
	}

	// démarrage
	if ($run === 'start') {
		$SpipSourcesIndexer->resetIndexesStats();
		set_request('run', 'continue');
	}

	// changement de source d’indexation
	$_source = $sources->get($source);
	if ($source != _request('current_source')) {
		set_request('current_source', $source);
		set_request('current_items', 0);
		set_request('current_total', $_source->countAllDocuments());
		set_request('current_source_index', array_search($source, array_keys(iterator_to_array($sources))));
		return formulaire_indexer_loop();
	}

	if ($run === 'start') {
		return formulaire_indexer_loop();
	}

	if ($run === 'continue') {
		$res = $SpipSourcesIndexer->indexerSource($_source);
		if (!$res['ok']) {
			set_request('run', null);
			return [
				'message_erreur' => sprintf('Une erreur est survenue : %s', $res['message']),
			];
		}
		set_request('current_items', $res['stats']['items']);
		if (!$res['reload']) {
			// source suivante ou fin ?
			if (_request('all')) {
				$source = array_keys(iterator_to_array($sources))[ _request('current_source_index') + 1 ] ?? null;
				if ($source) {
					set_request('source', $source);
					set_request('current_source_index', _request('current_source_index') + 1);
				} else {
					set_request('source', null);
					set_request('run', 'end');
					$SpipSourcesIndexer->resetIndexesStats();
				}
			} else {
				set_request('source', null);
				set_request('run', 'end');
				$SpipSourcesIndexer->resetIndexesStats();
			}
		}

		return formulaire_indexer_loop();
	}

	return [];
}

function formulaire_indexer_loop(): array {
	return [
		'message_erreur' => '',
		'scripts' => <<<JAVASCRIPT
		<script>
		jQuery(document).ready(function() {
			const form_indexer = document.querySelector(".formulaire_indexer_contenus form");
			jQuery(form_indexer).submit();
		});
		</script>
		JAVASCRIPT,
	];
}


function formulaires_indexer_contenus_traiter_dist() {

	$run = _request('run');
	set_request('run', null);

	$js = '<script type="text/javascript">if (window.jQuery) ajaxReload("navigation");</script>';

	if ($run === 'purge') {
		include_spip('inc/indexer');
		$indexer = indexer_indexer();
		$ok = $indexer->purgeDocuments();

		if ($ok) {
			return [
				'message_ok' => 'Tous les documents indexés ont été effacés.' . $js,
				'editable' => true,
			];
		}

		return [
			'message_ok' => 'Une erreur est survenue lors de la purge des documents indexés.',
			'editable' => true,
		];
	}

	return [
		'message_ok' => 'Fin de l’indexation' . $js,
		'editable' => true,
	];
}