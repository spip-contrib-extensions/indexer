<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function genie_indexer_optimiser_dist($date) {
	include_spip('inc/indexer');

	// ne pas lancer des operations inutiles sur un site local qui n'a pas moteur branche
	if (defined('_INDEXER_OFF') && _INDEXER_OFF) {
		return 1;
	}

	include_spip('inc/indexer');
	indexer_optimiser();

	return 1;
}
