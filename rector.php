<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\LogicalAnd\LogicalToBooleanRector;
use Rector\Config\RectorConfig;

return RectorConfig::configure()
	->withPaths([
		__DIR__ . '/action',
		__DIR__ . '/formulaires',
		__DIR__ . '/genie',
		__DIR__ . '/inc',
		__DIR__ . '/indexer',
		__DIR__ . '/iterateur',
		__DIR__ . '/lib/Indexer',
		__DIR__ . '/lib/Sphinx',
		__DIR__ . '/selecteurs',
		__DIR__ . '/Sources',
		__DIR__ . '/spip-cli',
	])
	->withRootFiles()
	->withPhpSets(php74: true)
	#->withPreparedSets(deadCode: true, codeQuality: true)
	->withRules([LogicalToBooleanRector::class])
;
