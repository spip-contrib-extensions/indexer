<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class IndexerOptimiser extends Command
{
	protected function configure() {
		$this
			->setName('indexer:optimiser')
			->setDescription('Optimiser l’index')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		include_spip('inc/indexer');

		// Appeler la fonction qui donne l'indexeur configuré pour ce SPIP
		$output->writeln("Optimisation de l'index...");
		indexer_optimiser();
		$output->writeln('Terminé.');

		return self::SUCCESS;
	}
}
