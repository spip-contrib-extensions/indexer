<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class IndexerPurger extends Command
{
	protected function configure() {
		$this
			->setName('indexer:purger')
			->setDescription('Purger l’indexation des contenus SPIP de ce site.')
			->setAliases(['indexer:purge'])
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		include_spip('inc/indexer');
		$indexer = indexer_indexer();
		$ret = $indexer->purgeDocuments();
		if (!$ret) {
			$this->io->text('<error>Erreur lors de la requete</error>');
			return self::FAILURE;
		}

		return self::SUCCESS;
	}
}
