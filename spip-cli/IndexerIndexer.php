<?php

use Indexer\Indexer;
use Indexer\Sources\Document;
use Indexer\Sources\SourceGeneratorInterface;
use Indexer\Sources\SourceInterface;
use Indexer\Sources\Sources;
use Spip\Cli\Console\Command;
use Spip\Indexer\Sources\SpipDocuments;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class IndexerIndexer extends Command
{
	protected function configure() {
		$this
			->setName('indexer:indexer')
			->setDescription('Lancer l’indexation des contenus SPIP configurés.')
			->addOption('table', null, InputOption::VALUE_OPTIONAL, 'Indexer une ou plusieurs tables')
			->addOption('source', null, InputOption::VALUE_OPTIONAL, 'Indexer une source')
			->addUsage('')
			->addUsage('--table articles')
			->addUsage('--table articles,rubriques')
			->addUsage('--source hierarchie_rubriques')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		$io = new SymfonyStyle($input, $output);
		$io->title('Indexation des contenus');

		$tables = null;
		if ($input->getOption('table')) {
			if (!$tables = $this->getTables($input->getOption('table'))) {
				$io->error('L’option <info>table</info> est vide');
				return self::FAILURE;
			} else {
				$io->comment('Tables sélectionnées:');
				$io->listing($tables);
			}
		}

		include_spip('inc/indexer');
		include_spip('base/objets');
		include_spip('inc/config');

		$sources = $this->getSources($tables);
		if ($input->getOption('source')) {
			$source_key = $input->getOption('source');
			if (!$sources->has($source_key)) {
				$io->error(sprintf('Source \'%s\' inconnue', $source_key));
				$io->info('Sources connues:');
				$io->listing(array_keys(iterator_to_array($sources)));
				return self::FAILURE;
			}

			$source = $sources->get($source_key);
			$sources = new Sources();
			$sources->register($source_key, $source);
		}


		spip_timer('indexer');
		ecrire_meta('indexer_derniere_reindexation', time());

		$ok = $this->indexer($io, $sources);

		$io->writeln(sprintf('Indexation en : <info>%s</info>', $this->humanDuration(spip_timer('indexer', true))));
		$io->writeln('');

		return $ok ? self::SUCCESS : self::FAILURE;
	}

	/**
	 * @return bool
	 *  - true Toutes les indexations sont réussies
	 *  - false Certaines indexations ont échoué
	 */
	protected function indexer(SymfonyStyle $io, Sources $sources): bool {
		// Appeler la fonction qui donne l'indexeur configuré pour ce SPIP
		$indexer = indexer_indexer();

		$io->writeln(sprintf('Indexer <info>%s</info> sources', count(iterator_to_array($sources))));

		$ok = true;
		foreach ($sources as $key => $source) {
			$io->section(sprintf('Source <info>%s</info>', $key));
			$ok &= $this->indexerSource($io, $source, $indexer);
		}
		return $ok;
	}

	/**
	 * @return bool
	 *  - true Toutes les indexations sont réussies
	 *  - false Certaines indexations ont échoué
	 */
	protected function indexerSource(SymfonyStyle $io, SourceInterface $source, Indexer $indexer): bool {
		if ($source instanceof SourceGeneratorInterface) {
			$num = $source->countAllDocuments();
			if ($num) {
				$documents = $source->yieldAllDocuments();
			}
		} else {
			// potential memory issues with big datasets !
			$documents = $source->getAllDocuments();
			$num = count($documents);
		}

		#$io->writeln(sprintf('<info>%s</info> %s à indexer', $num, $num > 1 ? 'documents' : 'document'));
		$ok = true;
		if ($num) {
			$errors = 0;
			/** @var Document $document */
			foreach ($io->progressIterate($documents, $num) as $document) {
				$_ok = $indexer->replaceDocument($document);
				if (!$_ok) {
					$ok = false;
					$io->error(sprintf('Echec d’indexation du document %s : %s', $document->id, $document->title));
					if (++$errors > 20) {
						$io->error(sprintf('Trop d’erreurs d’indexations… On coupe la boucle !'));
						break;
					}
				}
			};
		}
		return $ok;
	}

	private function getTables(string $tables): array {
		$tables = explode(',', $tables);
		$tables = array_map('trim', $tables);
		$tables = array_filter($tables);
		return $tables;
	}

	private function getSources(?array $tables): Sources {
		if ($tables === null) {
			return indexer_sources();
		}

		// On crée la liste des sources
		$sources = new Sources();

		// On ajoute chaque objet configuré aux sources à indexer
		// Par défaut on enregistre les articles s'il n'y a rien
		foreach ($tables as $table) {
			$sources->register(table_objet($table), new SpipDocuments(objet_type($table)));
		}

		return $sources;
	}

	private function humanDuration(float $milliseconds): string {
		$minutes = (int) floor($milliseconds / (60 * 1000));
		$seconds = (int) floor(($milliseconds % (60 * 1000)) / 1000);
		if ($seconds > 1) {
			$seconds = sprintf('%s secondes', $seconds);
		} else {
			$seconds = sprintf('%s seconde', $seconds);
		}
		if ($minutes > 1) {
			return sprintf('%s minutes, %s', $minutes, $seconds);
		}
		if ($minutes === 1) {
			return sprintf('%s minute, %s', $minutes, $seconds);
		}
		return $seconds;
	}
}
