<?php

use Indexer\Indexer;
use Indexer\Sources\SourceGeneratorInterface;
use Indexer\Sources\Sources;
use Indexer\Storage\Sphinx;
use Sphinx\SphinxQL\SphinxQLSingleton;
use Spip\Indexer\Sources\HierarchieMots;
use Spip\Indexer\Sources\HierarchieRubriques;
use Spip\Indexer\Sources\SpipDocuments;

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Constantes pour connexion à Sphinx
defined('SPHINX_SERVER_HOST') || define('SPHINX_SERVER_HOST', '127.0.0.1');
defined('SPHINX_SERVER_PORT') || define('SPHINX_SERVER_PORT', 9306);
defined('SPHINX_DEFAULT_INDEX') || define('SPHINX_DEFAULT_INDEX', 'spip');

// Charge les classes possibles de l'indexer
if (!class_exists(Indexer::class)) {
	require_once _DIR_PLUGIN_INDEXER . 'lib/vendor/autoload.php';
}

/**
 * Renvoyer un indexeur configuré avec un (et peut-être un jour plusieurs) lieu de stockage des choses à indexer
 *
 * Par défaut renvoie l'indexeur avec le stockage Sphinx intégré en interne et les paramètres des define()
 *
 * @pipeline_appel indexer_indexer
 * @return Indexer Objet ayant été configuré avec la méthode registerStorage()
 */
function indexer_indexer() {
	static $indexer = null;

	if ($indexer === null) {
		// On crée un indexeur
		$indexer = new Indexer();

		// On tente de le configurer avec Sphinx et les define()
		try {
			$indexer->registerStorage(
				new Sphinx(
					SphinxQLSingleton::getInstance(SPHINX_SERVER_HOST, SPHINX_SERVER_PORT),
					SPHINX_DEFAULT_INDEX
				)
			);
		} catch (\Exception $e) {
			if (!$message = $e->getMessage()) {
				$message = _L('Erreur inconnue');
			}
			die("<p class='erreur'>$message</p>");
		}

		// On le fait passer dans un pipeline
		$indexer = pipeline('indexer_indexer', $indexer);
	}

	return $indexer;
}

/**
 * Renvoyer les sources de données disponibles dans le site
 *
 * Un pipeline "indexer_sources" est appelé avec la liste par défaut, permettant de retirer ou d'ajouter des sources.
 *
 * @pipeline_appel indexer_sources
 * @return Sources Retourne un objet Sources listant les sources enregistrées avec la méthode register()
 */
function indexer_sources() {
	static $sources = null;

	if ($sources === null) {
		include_spip('base/objets');
		include_spip('inc/config');

		// On crée la liste des sources
		$sources = new Sources();

		// Toute la hiérarchie des rubriques
		$sources->register('hierarchie_rubriques', new HierarchieRubriques());

		// Toute la hiérarchie des mots
		if (_DIR_PLUGIN_MOTS) {
			$sources->register('hierarchie_mots', new HierarchieMots());
		}

		// On ajoute chaque objet configuré aux sources à indexer
		// Par défaut on enregistre les articles s'il n'y a rien
		foreach (lire_config('indexer/sources_objets', ['spip_articles']) as $table) {
			if ($table) {
				$sources->register(table_objet($table), new SpipDocuments(objet_type($table)));
			}
		}

		// On passe les sources dans un pipeline
		$sources = pipeline('indexer_sources', $sources);
	}

	return $sources;
}

function indexer_sources_with_generator() {
	$sources_generator = new Sources();
	foreach (indexer_sources() as $key => $source) {
		if ($source instanceof SourceGeneratorInterface) {
			$sources_generator->register($key, $source);
		}
	}
	return $sources_generator;
}

/**
 * Indexer une partie des documents d'une source.
 *
 * Cette fonction est utilisable facilement pour programmer un job.
 *
 * @param string $alias_de_sources Alias donné à la source enregistrée dans les Sources du site
 * @param mixed $start Début des documents à indexer
 * @param mixed $end Fin des documents à indexer
 */
function indexer_job_indexer_source($alias_de_sources, $start, $end) {
	// On va chercher l'indexeur du SPIP
	$indexer = indexer_indexer();
	// On va chercher les sources du SPIP
	$sources = indexer_sources();
	// On récupère celle demandée en paramètre
	// les alias de source correpondent en principe a la table (ex 'articles'), donc si $alias_de_sources est un objet ('article') on ne va pas trouver la source
	if (!function_exists('table_objet')) {
		include_spip('base/objets');
	}
	if (
		($source = $sources->get($alias_de_sources))
		|| ($source = $sources->get(table_objet($alias_de_sources)))
	) {
		// On va chercher les documents à indexer
		if ($source instanceof SourceGeneratorInterface) {
			$documents = $source->yieldDocuments($start, $end);
		} else {
			$documents = $source->getDocuments($start, $end);
		}
		// Et on le remplace (ou ajoute) dans l'indexation
		$res = $indexer->replaceDocuments($documents);

		// en cas d'erreur, on se reprogramme pour une autre fois
		if (!$res) {
			job_queue_add(
				'indexer_job_indexer_source',
				"Replan indexation $alias_de_sources $start $end",
				[$alias_de_sources, $start, $end, uniqid()],
				'inc/indexer',
				false, // duplication possible car ce job-ci n'est pas encore nettoyé
				time() + 15 * 60, // attendre 15 minutes
				-10 // priorite basse
			);
		}
	} else {
		spip_log("indexer_job_indexer_source: Source $alias_de_sources inconnue", 'indexer' . _LOG_ERREUR);
	}
}

/**
 * Optimiser l'index
 * appeler par le genie ou le cli
 */
function indexer_optimiser() {
	$sphinxql = SphinxQLSingleton::getInstance(SPHINX_SERVER_HOST, SPHINX_SERVER_PORT);
	$sphinxql->query('optimize index ' . SPHINX_DEFAULT_INDEX);
}

function indexer_statistiques_indexes_depuis($date_reference = null) {
	if (!$date_reference) {
		$date_reference = intval(lire_meta('indexer_derniere_reindexation'));
	}

	$query = "SELECT COUNT(*) AS c,
	properties.objet AS objet,
	(date_indexation > $date_reference) AS recent,
	properties.source AS source
	FROM " . SPHINX_DEFAULT_INDEX . '
	GROUP BY recent, properties.objet, properties.source';

	$sphinx = SphinxQLSingleton::getInstance(SPHINX_SERVER_HOST, SPHINX_SERVER_PORT);

	$all = $sphinx->allfetsel($query);

	if (
		!is_array($all)
		|| !is_array($all['query'])
		|| !is_array($all['query']['docs'])
	) {
		echo '<p class=error>' . _L('Erreur Sphinx') . '</p>';
	} else {
		$liste = [];
		foreach ($all['query']['docs'] as $l) {
			$liste[$l['source']][table_objet($l['objet'])][$l['recent']] = intval($l['c']);
		}
		return $liste;
	}
}

/**
 * @deprecated 4.0 Utiliser spip-cli pour indexer
 */
function indexer_tout_reindexer_async($bloc = 100) {

	foreach (indexer_lister_blocs_indexation($bloc) as $alias => $ids) {
		foreach ($ids as $id) {
			$start = $id;
			$end = $id + $bloc - 1;
			job_queue_add(
				'indexer_job_indexer_source',
				'Indexer ' . $alias . ' de ' . $start . ' à ' . $end,
				[$alias, $start, $end],
				'inc/indexer',
				true // pas de duplication
			);
		}
	}

	ecrire_meta('indexer_derniere_reindexation', time());
}

/**
 * @deprecated 4.0 Utiliser spip-cli pour indexer
 */
function indexer_lister_blocs_indexation($bloc = 100) {

	// On récupère toutes les sources compatibles avec l'indexation
	$sources = indexer_sources();

	$liste = [];

	foreach ($sources as $alias => $source) {
		// Si une méthode pour définir explicitement existe, on l'utilise
		if (method_exists($source, 'getObjet')) {
			$objet_source = $source->getObjet();
		}
		// Sinon on cherche avec l'alias donné à la source
		else {
			$objet_source = objet_type(strtolower($alias));
		}

		// alias = mots, objet_source = mot
		// alias = hierarchie_rubriques, objet_source = rubrique
		$liste[$alias] = [];

		$parts = new \ArrayIterator($source->getParts($bloc));

		while ($parts->valid()) {
			$part = $parts->key();
			$liste[$alias][] = $part * $bloc;
			$parts->next();
		}
	}

	return array_filter($liste);
}

// suggerer des mots (si aspell est installee)
// code adapté de https://github.com/splitbrain/dokuwiki-plugin-spellcheck/blob/master/spellcheck.php
function indexer_suggestions($mot) {
	include_spip('lib/aspell');

	if (!class_exists('Aspell')) {
		spip_log('pas trouvé aspell.php', 'indexer');
		return [];
	}

	try {
		$spell = new Aspell($GLOBALS['spip_lang'], null, 'utf-8');
		// $spell->setMode(PSPELL_FAST);
		if (!$spell->runAspell($mot, $out, $err, ['!', '+html', '@nbsp'])) {
			spip_log('An error occurred while trying to run the spellchecker: ' . $err, 'indexer');
			return null;
		}
	} catch (Exception $e) {
		spip_log($e, 'indexer');
		return null;
	}

	// go through the result
	$lines = explode("\n", $out);
	$rcnt = count($lines) - 1;    // aspell result count
	for ($i = $rcnt; $i >= 0; $i--) {
		$line = trim($lines[$i]);
		if ($line[0] == '@') {
			continue;
		} // comment
		if ($line[0] == '*') {
			continue;
		} // no mistake in this word
		if ($line[0] == '+') {
			continue;
		} // root of word was found
		if ($line[0] == '?') {
			continue;
		} // word was guessed
		if (empty($line)) {
			continue;
		}
		// now get the misspelled words
		if (preg_match('/^& ([^ ]+) (\d+) (\d+): (.*)/', $line, $match)) {
			// match with suggestions
			$word = $match[1];
			$off = $match[3] - 1;
			$sug = preg_split('|, |', $match[4]);
		} elseif (preg_match('/^# ([^ ]+) (\d+)/', $line, $match)) {
			// match without suggestions
			$word = $match[1];
			$off = $match[2] - 1;
			$sug = null;
		} else {
			// couldn't parse output
			spip_log("The spellchecker output couldn't be parsed line $i '$line'", 'indexer');
			return null;
		}
	}

	// aspell peut nous renvoyer des mots accentués
	// qui auront la même valeur dans sphinx,
	// il faut donc unifier
	// ne pas non plus accepter de mots avec apostrophe
	$suggests = [];
	if (is_array($sug)) {
		foreach ($sug as $t) {
			if (strpos($t, "'") === false) {
				$s = translitteration($t);
				$suggests[$s] = $t;
			}
		}
		$sug = $suggests;
	}

	return $sug;
}

// trier les mots par nombre d'occurrences reelles dans la base Sphinx
// et supprimer ceux qui n'y figurent pas
// on se base sur la forme exacte (=mot) ; et sans espaces ni tirets !
function indexer_motiver_mots($mots) {
	$liste = [];
	foreach ($mots as $i => $m) {
		$mots[$i] = '=' . preg_replace('/\W/', '', $m);
	}
	$m = join(' ', $mots);
	$query = 'SELECT id FROM ' . SPHINX_DEFAULT_INDEX . " WHERE MATCH('$m') LIMIT 1";

	$sphinx = SphinxQLSingleton::getInstance(SPHINX_SERVER_HOST, SPHINX_SERVER_PORT);
	$all = $sphinx->allfetsel($query);

	if (
		!is_array($all)
		|| !is_array($all['query'])
		|| !is_array($all['query']['meta'])
	) {
		// echo "<p class=error>" . _L('Erreur Sphinx')."</p>";
	} else {
		if (is_array($all['query']['meta']['keywords'])) {
			foreach ($all['query']['meta']['keywords'] as $i => $w) {
				$translitt = str_replace('=', '', $w['keyword']);
				if (intval($w['docs']) > 3) {
					$liste[$translitt] = intval($w['docs']);
				}
			}
			$liste = array_filter($liste);
			arsort($liste);
		}
		return array_keys($liste);
	}
}

// compare une liste de suggestions au contenu indexé dans la base sphinx
function indexer_suggestions_motivees($mot) {
	$sug = indexer_suggestions($mot);
	if (is_array($sug) && count($sug) > 0) {
		$sug = indexer_motiver_mots($sug);
	}
	return $sug;
}

/*
 * faire un DUMP SQL de notre base sphinx
 * index: nom de l'index (table) a dumper
 * format: format de sortie ("sphinx" ou "mysql")
 * dest: fichier destination (null: stdout)
 * bloc : nombre d'enregistrements a rapatrier a chaque tour (maximum 1000)
 * usage :
	  include_spip('inc/indexer');
	  indexer_dumpsql();
	  // indexer_dumpsql(SPHINX_DEFAULT_INDEX, 'tmp/indexer.sql', 1000);
*/
function indexer_dumpsql($index = null, $format = 'sphinx', $dest = null, $bloc = 100) {
	if ($dest === null) {
		$dest = 'php://stdout';
	}

	$fp = fopen($dest, 'w');
	if (!$fp) {
		spip_log('Impossible d ouvrir ' . $dest, 'indexer');
		return false;
	}

	$sphinx = SphinxQLSingleton::getInstance(SPHINX_SERVER_HOST, SPHINX_SERVER_PORT);

	include_spip('inc/config');
	$version = lire_config('plugin/INDEXER/version');

	if ($index === null) {
		$index = SPHINX_DEFAULT_INDEX;
	}

	$comm = '# SPIP indexer / SphinxQL Dump
# version ' . $version . '
# https://contrib.spip.net/Indexer
#
# Host: ' . SPHINX_SERVER_HOST . ':' . SPHINX_SERVER_PORT . '
# Generation Time: ' . date(DATE_ISO8601) . '
# Server version: (unknown)
# PHP Version: ' . PHP_VERSION . '
#
# Database : `' . $index . '`
#

';

	$query = 'DESC ' . $index;
	$all = $sphinx->allfetsel($query);

	if (isset($all['query']['docs'])) {
		$comm .= '# --------------------------------------------------------

#
# Table structure for table `' . $index . '`
#

CREATE TABLE `' . $index . '` (
';
		$fields = [];
		foreach ($all['query']['docs'] as $doc) {
			if ($format == 'sphinx') {
				$fields[] = "\t" . '`' . $doc['Field'] . '` ' . $doc['Type'];
			} elseif ($format == 'mysql') {
				switch ($doc['Type']) {
					case 'field':
						break;
					case 'bigint':
					case 'uint':
						$type = 'BIGINT(21) NOT NULL';
						$fields[] = "\t" . '`' . $doc['Field'] . '` ' . $type;
						break;
					case 'timestamp':
						$type = 'TINYTEXT DEFAULT \'\' NOT NULL';
						$fields[] = "\t" . '`' . $doc['Field'] . '` ' . $type;
						break;
					case 'json':
						$type = 'TEXT DEFAULT \'\' NOT NULL';
						$fields[] = "\t" . '`' . $doc['Field'] . '` ' . $type;
						break;
					case 'string':
						$type = 'LONGTEXT DEFAULT \'\' NOT NULL';
						$fields[] = "\t" . '`' . $doc['Field'] . '` ' . $type;
						break;
					case 'mva':
						$type = 'TINYTEXT DEFAULT \'\' NOT NULL';
						$fields[] = "\t" . '`' . $doc['Field'] . '` ' . $type;
						break;
				}
			}
		}
		$comm .= join(",\n", $fields);
		$comm .= '
);


#
# Dumping data for table `' . $index . '`
#

';
	}

	if (!fwrite($fp, $comm)) {
		spip_log('Impossible d ecrire dans ' . $dest, 'indexer');
		return false;
	}

	do {
		$where = isset($begin) ? " WHERE id > $begin " : '';
		$query = 'SELECT * FROM ' . $index . $where . " ORDER BY id ASC LIMIT 0,$bloc";

		$all = $sphinx->allfetsel($query);
		$cdocs = count($all['query']['docs']);
		if ($cdocs > 0) {
			foreach ($all['query']['docs'] as $doc) {
				$sql = 'INSERT INTO ' . $index . ' ('
					. join(', ', array_keys($doc))
					. ') VALUES ('
					. join(', ', array_map('_q', $doc))
					. ');' . "\n";

				if (!fwrite($fp, $sql)) {
					spip_log('Impossible d ecrire dans ' . $dest, 'indexer');
					return false;
				}
			}
			$begin = $all['query']['docs'][$cdocs - 1]['id'];
		}
	} while ($cdocs > 0);

	if ($fp) {
		fclose($fp);
	}
	return true;
}

/**
 * Indique juste si l’indexer se connecte au serveur d’indexation
 */
function indexer_can_connect(): bool {
	$sphinxql = SphinxQLSingleton::getInstance(SPHINX_SERVER_HOST, SPHINX_SERVER_PORT);
	$test = $sphinxql->allfetsel('SELECT 1');
	if (!$test || empty($test['query']['docs'][0]['1'])) {
		return false;
	}
	$test = $test['query']['docs'][0]['1'];
	return $test === '1';
}

/**
 * Retourne une version et le type de moteur supposé
 */
function indexer_get_db_version(): ?array {
	$sphinxql = SphinxQLSingleton::getInstance(SPHINX_SERVER_HOST, SPHINX_SERVER_PORT);
	$version = $sphinxql->allfetsel('SELECT VERSION() as version;');
	if (!$version || empty($version['query']['docs'][0]['version'])) {
		return null;
	}
	$version = $version['query']['docs'][0]['version'];
	$version = explode(' ', $version, 2)[0];
	return [
		'version' => $version,
		'moteur' => 'manticore',
	];
}
